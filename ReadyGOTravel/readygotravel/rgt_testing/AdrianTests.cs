﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using readygotravel.Models;
using readygotravel.Controllers;

//PBI 268 - Adrian Unit Test
namespace rgt_testing
{
    [TestClass]
    public class AdrianTests

    {
        [TestMethod]
            public void ConfirmsCorrectRemainderAmount_ShouldCalculateToTheDecimal()
        {
            //Arrange
            //Sum of total costs: 1679.37 + 1506.21 + 800.05 = 3985.63
            //User Amount - total costs: 5000.29 - 3985.63 = 1014.66
            Search testSearchItem = getTestObject();
            testSearchItem.MaxAmount = 5000.29m;
            decimal testAirportCost = 800.05m;
            decimal testHotelCost = 1506.21m;
            decimal testFoodCost = 1679.37m;
            
            //Act
            decimal testTotalRemaing = 5000.29m - 3985.63m;
            decimal testRemainingAmountFunction = GetRemainingAmount(testSearchItem,
                testAirportCost, testHotelCost, testFoodCost);
            
            //Assert
            Assert.AreEqual(testTotalRemaing, testRemainingAmountFunction);
            Assert.AreEqual(1014.66m, testRemainingAmountFunction);

        }

        [TestMethod]
        public void ConfirmsThatFunctionReturnsNegativeAmount_ShouldBeInitalMinusCosts()
        {
            //Arrange
            Search testSearchItem = getTestObject();
            testSearchItem.MaxAmount = 1m;
            decimal testAirportCost = 200m;
            decimal testHotelCost = 300m;
            decimal testFoodCost = 50m;

            //Act
            decimal testTotalRemaing = 1 - 550m;
            decimal testRemainingAmountFunction = GetRemainingAmount(testSearchItem,
                testAirportCost, testHotelCost, testFoodCost);

            //Assert
            Assert.AreEqual(testTotalRemaing, testRemainingAmountFunction);
            Assert.AreEqual(-549m, testRemainingAmountFunction);

        }

        [TestMethod]
        public void ConfirmsThatFunctionAddressesNegativeInput_ShouldCalculateCorrectly()
        {
            //Arrange
            Search testSearchItem = getTestObject();
            testSearchItem.MaxAmount = -100m;
            decimal testAirportCost = -200m;
            decimal testHotelCost = -300m;
            decimal testFoodCost = -50m;

            //Act
            decimal testTotalRemaing = -100 - 550m;
            decimal testRemainingAmountFunction = GetRemainingAmount(testSearchItem,
                testAirportCost, testHotelCost, testFoodCost);

            //Assert
            Assert.AreEqual(testTotalRemaing, testRemainingAmountFunction);
            Assert.AreEqual(-650m, testRemainingAmountFunction);

        }

        public Search getTestObject()
        {
            Search testResultObject = new Search();
            return testResultObject;
        }

        private decimal GetRemainingAmount(Search search, decimal flight, decimal hotel, decimal food)
        {
            decimal amount = search.MaxAmount;
            if (search.MaxAmount < 0)
            {
                amount = amount + (flight + hotel + food);
                amount = Math.Round(amount, 2);
            }
            else
            {
                amount = amount - (flight + hotel + food);
                amount = Math.Round(amount, 2);
            }
            return amount;
        }
    }
}
