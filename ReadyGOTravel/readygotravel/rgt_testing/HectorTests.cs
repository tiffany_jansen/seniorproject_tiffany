﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

//PBI 267 - Hector Unit Test
namespace rgt_testing
{
    /// <summary>
    /// Summary description for HectorTests
    /// </summary>
    [TestClass]
    public class HectorTests
    {
        [TestMethod]
        public void CheckCrimeRate_ReturnsNumberRankingOfLowCrimeRate_1()
        {
            //Arrange
            decimal crimeRateValue = 100;
            int expectedCrimeRank = 1;

            //Act
            int CrimeRanking = CheckCrime(crimeRateValue);

            //Assert
            Assert.AreEqual(expectedCrimeRank, CrimeRanking);
        }

        [TestMethod]
        public void CheckCrimeRate_ReturnsNumberRankingOfMediumLowCrimeRate_2()
        {
            //Arrange
            decimal crimeRateValue = 300;
            int expectedCrimeRank = 2;

            //Act
            int CrimeRanking = CheckCrime(crimeRateValue);

            //Assert
            Assert.AreEqual(expectedCrimeRank, CrimeRanking);
        }

        [TestMethod]
        public void CheckCrimeRate_ReturnsNumberRankingOfMediumHighCrimeRate_3()
        {
            //Arrange
            decimal crimeRateValue = 450;
            int expectedCrimeRank = 3;

            //Act
            int CrimeRanking = CheckCrime(crimeRateValue);

            //Assert
            Assert.AreEqual(expectedCrimeRank, CrimeRanking);
        }

        [TestMethod]
        public void CheckCrimeRate_ReturnsNumberRankingOfHighCrimeRate_4()
        {
            //Arrange
            decimal crimeRateValue = 1000;
            int expectedCrimeRank = 4;

            //Act
            int CrimeRanking = CheckCrime(crimeRateValue);

            //Assert
            Assert.AreEqual(expectedCrimeRank, CrimeRanking);
        }

        private int CheckCrime(decimal crimeRate)
        {
            if (crimeRate > 550)
                return 4;
            else if (crimeRate > 400)
                return 3;
            else if (crimeRate > 250)
                return 2;
            else
                return 1;
        }

    }
}
