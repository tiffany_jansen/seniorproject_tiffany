﻿INSERT INTO [dbo].[Countries] (CountryName) VALUES
('United States');

INSERT INTO [dbo].[States] (StateName, CountryID, FoodCost, FoodItemOne, FoodItemTwo, FoodItemThree, SpringTemp, SummerTemp, FallTemp, WinterTemp, SpringWeather, SummerWeather, FallWeather, WinterWeather, CrimeRate) VALUES
('ALABAMA', 1, 34.85,'Fried Chicken', 'Ribs', 'Baked Grits', 77, 91, 78, 59, 'Rainy', 'Humid', 'Cloudy', 'Rainy', 524.2), ('ALASKA', 1, 43.05, 'Grilled Salmon', 'King Crab', 'Reindeer Sausage', 44, 64, 41, 25, 'Snowy', 'Cloudy', 'Cloudy', 'Snowy', 829), ('ARIZONA', 1,39.36, 'Chimichangas', 'Sonoran Hot Dogs', 'Carne Seca Plate', 85, 104, 88, 68, 'Sunny', 'Sunny', 'Sunny', 'Sunny', 508),
('ARKANSAS', 1,35.26, 'Hubcap Burger', 'Eggplant Casserole', 'Barbecue Pork', 72, 84, 66, 43, 'Cloudy', 'Humid', 'Cloudy', 'Rainy', 554.9), ('CALIFORNIA', 1,45.92, 'Carne Asada Burrito', 'Fish Tacos', 'In-N-Out Double-Double', 72, 84, 66, 43, 'Sunny', 'Sunny', 'Sunny', 'Cloudy', 449.3), ('COLORADO', 1,42.23, 'Lamb Fondue', 'Rocky Mountain Oysters', 'Lobster Macaroni and Cheese', 62, 86, 65, 45, 'Clear skies', 'Sunny', 'Clear skies', 'Clear skies', 368.1), 
('CONNECTICUT', 1,44.28, 'White Clam Pizza', 'Ricotta Gnocchi', 'Steamed Cheeseburger',  57, 80, 63, 39, 'Clear skies', 'Humid', 'Cloudy', 'Cloudy', 228), ('DELAWARE', 1,41.00, 'Chicken and Slippery Dumplings', 'Blue Claw Crabs', 'Grotto Pizza', 65, 85, 68, 45, 'Cloudy', 'Humid', 'Cloudy', 'Clear skies', 453.4), ('FLORIDA', 1,40.59, 'Stone Crabs', 'Cuban Sandwich', 'Gator Tail', 80, 91, 80, 65, 'Clear skies', 'Humid', 'Clear skies', 'Clear skies', 408), 
('GEORGIA', 1,37.72, 'Chicken & Dumplings', 'Fried Chicken', 'Fried Green Tomatoes', 64, 88, 73, 54, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy', 357.2), ('HAWAII', 1,47.56, 'Poke', 'Garlic Shrimp', 'Salted Dried Beef', 83, 88, 86, 80, 'Sunny', 'Humid', 'Humid', 'Sunny', 250.6), ('IDAHO', 1,38.13, 'Ruby Red Trout', 'Finger Steaks', 'Idaho Spuds', 63, 87, 64, 40, 'Cloudy', 'Sunny', 'Cloudy', 'Cloudy', 226.4),
('ILLINOIS', 1,41.00, 'Bacon-Wrapped Chorizo-Stuffed Dates', 'Deep-Dish Pizza', 'Baby Back Ribs', 58, 82, 62, 34, 'Cloudy', 'Humid', 'Cloudy', 'Rainy', 438.8), ('INDIANA', 1,36.90, 'Biscuits and Gravy', 'Roast Beef', 'German Sausages', 62, 83, 64, 38, 'Cloudy', 'Humid', 'Cloudy', 'Cloudy', 399), ('IOWA', 1, 36.49, 'Breaded Pork Tenderloin', 'Taco Pizza', 'Panino', 61, 84, 62, 33, 'Cloudy', 'Humid', 'Cloudy', 'Rainy', 293.4), 
('KANSAS', 1,36.49, 'Barbecue Ribs', 'Chili and Cinnamon Rolls', 'Barbecue Hot Wings', 66, 87, 67, 42, 'Clear skies', 'Humid', 'Clear skies', 'Clear skies', 413), ('KENTUCKY', 1,35.67, 'Benedictine Sandwhich', 'Goetta', 'Burgoo Stew', 65, 85, 67, 43, 'Rainy', 'Humid', 'Cloudy', 'Cloudy',225.8), ('LOUISIANA', 1,36.90, 'Jambalaya', 'Ya-Ka-Mein', 'Fried Seafood Sub Sandwhich', 78, 90, 79, 63, 'Cloudy', 'Humid', 'Clear skies', 'Clear skies', 557), 
('MAINE', 1,40.18, 'Clambake Oysters', 'Lobster Pie', 'Baked Beans & Hot Dogs', 52, 77, 57, 31, 'Cloudy', 'Clear skies', 'Cloudy', 'Cloudy',121), ('MARYLAND', 1,44.69, 'Rockfish', 'Crab Cakes', 'Lean Top Roast Beef', 64, 87, 68, 44, 'Cloudy', 'Humid', 'Clear skies', 'Clear skies', 500.2), ('MASSACHUSETTS', 1,43.46, 'Scallops', 'Corned Beef', 'Clam Chowder', 55, 79, 61, 38, 'Cloudy', 'Sunny', 'Clear skies', 'Clear skies', 358), 
('MICHIGAN', 1,38.13, 'Detroit-Style Pizza', 'Meat Hand Pies', 'Greek Salad', 57, 80, 60, 32, 'Clear skies', 'Sunny', 'Clear skies', 'Cloudy', 450), ('MINNESOTA', 1, 39.77, 'Swedish Meatballs', 'Wild Rice Soup', 'Chicken Casserole', 57, 82, 58, 28, 'Cloudy', 'Rainy', 'Clear skies', 'Snowy', 238.3), ('MISSISSIPPI', 1,34.44, 'Catfish and Hush Puppies', 'Crawfish', 'Fried Chicken', 76, 91, 77, 58, 'Cloudy', 'Humid', 'Clear skies', 'Clear skies', 285.7),
('MISSOURI', 1,36.08, 'Salami Sandwich', 'St. Louis-Style Pizza', 'Toasted Ravioli', 66, 86, 68, 42, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy', 530.3), ('MONTANA', 1,38.95, 'Chicken-Fried Steak', 'Prime Rib', 'Bison-Based Stew', 57, 82, 58, 34, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy', 377.1), ('NEBRASKA', 1,36.90, 'Biscuits and Gravy', 'Macaroni and Cheese', 'Corned Beef Hash', 62, 86, 63, 35, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy', 305.9),
('NEVADA', 1,40.18, 'Elk Chops', 'Fry Bread Indian Tacos', 'Eggs Benedict', 79, 101, 80, 59, 'Sunny', 'Sunny', 'Sunny', 'Sunny', 555.9), ('NEW HAMPSHIRE', 1,43.05, 'Lobster and Steamers ', 'French Toast', 'Gravy Covered French Fries', 56, 80, 60, 34, 'Cloudy', 'Clear skies', 'Clear skies', 'Cloudy', 198.7), ('NEW JERSEY', 1,45.92, 'South Jersey Pizza', 'Sliders', 'Pork Roll Sandwich', 61, 84, 65, 41, 'Cloudy', 'Sunny', 'Clear skies', 'Clear skies', 228.8),
('NEW MEXICO', 1,38.54, 'Blue Corn Pancakes', 'Green Chile Cheeseburger', 'Carne Adovada', 65, 84, 66, 45, 'Clear skies', 'Sunny', 'Clear skies', 'Clear skies', 783.5), ('NEW YORK', 1,46.33, 'Coal Oven Pizza', 'Hot Dogs', 'Bagels and Lox', 60, 82, 65, 41, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy', 356.7), ('NORTH CAROLINA', 1,36.90,'Lexington-Style Barbecue', 'She-Crab Bisque', 'Shrimp and Grits', 71, 87, 71, 53, 'Cloudy', 'Humid', 'Cloudy', 'Rainy',363.7),  
('NORTH DAKOTA', 1,37.72, 'Tater Tot Casserole ', 'Cheese Buttons', 'Knoephla Soup', 55, 81, 56, 25, 'Rainy', 'Clear skies', 'Cloudy', 'Snowy', 281.3), ('OHIO', 1,36.08, 'Polish-Style Sausage Sandwhiches', 'Cincinnati Chili', 'Goetta Hash', 62, 83, 64, 39, 'Cloudy', 'Humid', 'Cloudy', 'Snowy', 297.5), ('OKLAHOMA', 1,36.49, 'Fried-Onion Burger', 'Pho Beef Broth and Rice Noodles', 'Chicken Waffles', 71, 91, 73, 52, 'Clear skies', 'Humid', 'Clear skies', 'Clear skies', 456.2),  
('OREGON', 1, 40.59, 'Veggie Burger', 'Albacore Tuna Melt', 'Bacon Maple Bar', 61, 79, 64, 48, 'Rainy', 'Sunny', 'Rainy', 'Rainy', 281.8), ('PENNSYLVANIA', 1, 40.18, 'Scrapple', 'Philly Cheesesteak', 'Mushroom Strudel', 63, 85, 67, 43, 'Cloudy', 'Humid', 'Clear skies', 'Rainy', 313.3), ('RHODE ISLAND', 1, 40.59, 'Pizza Strips', 'Clam Cakes', 'Johnnycakes', 58, 80, 63, 39, 'Cloudy', 'Sunny', 'Clear skies', 'Rainy', 232.2), 
('SOUTH CAROLINA', 1, 36.49, 'Fried Seafood', 'Grits', 'Barbecue', 76, 91, 76, 58, 'Cloudy', 'Humid', 'Clear skies', 'Rainy', 506.2), ('SOUTH DAKOTA', 1,35.67, 'Hot Beef Commercial', 'Indian Tacos', 'Meat-and-Vegetable Pies ', 55, 81, 56, 24, 'Cloudy', 'Sunny', 'Cloudy', 'Cloudy', 433.6), ('TENNESSEE', 1,36.49, 'Dry Ribs', 'Fried Pickles', 'Country Ham and Red-Eye Gravy', 72, 90, 74, 52, 'Cloudy', 'Humid', 'Clear skies', 'Rainy', 651.5),  
('TEXAS', 1,39.77, 'Brisket', 'Puffy Tacos', 'Chili', 79, 95, 81, 63, 'Clear skies', 'Humid', 'Sunny', 'Clear skies', 438.9), ('UTAH', 1, 39.77, 'Potato Tots', 'Corn Chowder', 'Wild Game Chili', 61, 87, 64, 40, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy', 238.9), ('VERMONT', 1,41.82, 'Chicken Pot Pie', 'Chili Dogs', 'Slow-Roasted Lamb Shanks', 54, 78, 57, 30, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy', 165.8),  
('VIRGINIA', 1, 41.82, 'Rappahannock Oysters', 'Ham & Biscuits', 'Brunswick Stew', 69, 88, 71, 49, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy', 208.2), ('WASHINGTON', 1,43.05, 'Smoked Sockeye Salmon', 'Oyster Stew', 'Beechers Mac & Cheese', 59, 74, 60, 47, 'Rainy', 'Sunny', 'Rainy', 'Rainy', 304.5), ('WEST VIRGINIA', 1,36.08, 'Pepperoni Rolls', 'Baked Steak and Gravy', 'Buckwheat Pancakes', 66, 83, 67, 45, 'Cloudy', 'Humid', 'Clear skies', 'Cloudy', 350.7),  
('WISCONSIN', 1, 38.13, 'Bratwurst', 'Roesti and Cheese Fondue', 'Swedish Pancakes', 53, 77, 58, 31, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy', 319.9), ('WYOMING', 1,39.36, 'Spicy Elk Sausage', 'Butterflied Trout', 'Biscuits and Gravy', 55, 79, 59, 39, 'Cloudy', 'Sunny', 'Clear skies', 'Cloudy', 237.5);

INSERT INTO [dbo].[Climates] (ClimateType) VALUES
('Semiarid Steppe'), ('Humid subtropical'), ('Marine westcoast'), ('Mediterranean'), ('Humid continental warm summer'), ('Humid continental cold summer'), ('Highland'), ('Tropical wet/dry'), ('Desert'), ('Tundra');

INSERT INTO [dbo].[Geographies] (GeographyType) VALUES
('Mountain'), ('River'), ('Lake'), ('Forest'), ('Coast'), ('Prairie'), ('Desert'), ('Mesa'), ('Swamp'), ('Plateau');

INSERT INTO [dbo].[StateClimates] (StateID, ClimateID) VALUES
(1, 2), (2, 10), (3, 1), (3, 7), (3, 9), (4, 2), (5, 4), (5, 7), (5, 9), (6, 1), (6, 7), (6, 9),  (7, 5), (8, 2), (9, 2), (9, 8), (10, 2), (11, 8), (12, 1), (12, 7), (13, 5), (13, 2), (14, 5), (14, 2), (15, 5), (15, 6), (16, 1), (16, 2), (16, 5), (17, 2), (17, 5), (18, 2), (19, 6), (20, 2), (20, 5), (21, 6), (22, 5), (22, 6), (23, 6), (24, 2), (25, 2), (25, 5), (26, 1), (26, 7), (27, 1), (27, 5), (27, 6), (28, 1), (28, 7), (28, 9), (29, 5), (29, 6), (30, 2), (30, 5), (31, 1), (31, 7), (31, 9), (32, 5), (32, 6), (33, 2), (33, 5), (34, 1), (34, 6), (35, 2), (35, 5), (36, 1), (36, 2), (37, 3), (37, 1), (37, 7), (38, 5), (39, 5), (40, 2), (41, 1), (41, 6), (42, 2), (42, 5), (43, 1), (43, 2), (44, 1), (44, 7), (44, 9), (45, 5), (45, 6), (46, 2), (46, 5), (47, 1), (47, 3), (47, 7), (48, 5), (49, 5), (49, 6), (50, 1), (50, 7);

INSERT INTO [dbo].[StateGeographies] (StateID, GeographyID) VALUES
(1, 1), (1, 2), (1, 3), (1, 5), (2, 1), (2, 2), (2, 5), (3, 1), (3, 2), (3, 7), (3, 8), (4, 1), (4, 2), (5, 1), (5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7), (6, 1), (6, 2), (6, 6), (7, 2), (7, 5), (7, 4), (8, 2), (8, 5), (8, 4), (9, 2), (9, 5), (9, 3), (9, 9), (10, 1), (10, 2), (10, 5), (10, 4), (10, 9), (11, 1), (11, 5), (12, 1), (12, 2), (12, 3), (12, 4), (12, 7), (13, 2), (13, 3), (13, 4), (13, 6), (14, 2), (14, 3), (14, 4), (14, 6), (15, 2), (15, 3), (15, 4), (15, 6), (16, 6), (16, 4), (16, 2), (17, 2), (17, 4), (17, 3), (18, 2), (18, 3), (18, 5), (18, 4), (18, 9), (19, 1), (19, 2), (19, 3), (19, 4), (19, 5), (20, 1), (20, 2), (20, 5), (20, 4), (21, 1), (21, 2), (21, 3), (21, 4), (21, 5), (22, 2), (22, 3), (22, 4), (23, 1), (23, 2), (23, 4), (23, 3), (24, 2), (24, 3), (24, 4), (24, 6), (25, 2), (25, 3), (25, 4), (25, 10), (26, 1), (26, 2), (26, 3), (26, 4), (26, 6), (27, 2), (27, 6), (28, 1), (28, 8), (28, 3), (28, 2), (28, 7), (29, 1), (29, 2), (29, 3), (29, 4), (29, 5), (30, 2), (30, 5), (30, 4), (31, 1), (31, 2), (31, 4), (31, 6), (31, 7), (32, 1), (32, 2), (32, 3), (32, 4), (32, 5), (33, 1), (33, 2), (33, 3), (33, 4), (33, 5), (34, 2), (34, 3), (34, 6), (34, 4), (35, 2), (35, 3), (35, 4), (35, 10), (36, 2), (36, 3), (36, 4), (36, 10), (36, 6), (37, 1), (37, 2), (37, 3), (37, 4), (37, 5), (37, 7), (38, 1), (38, 2), (38, 4), (39, 2), (39, 5), (39,4), (40, 1), (40, 2), (40, 3), (40, 4), (40, 5), (40, 9), (41, 1), (41, 2), (41, 3), (41, 4), (41, 6), (42, 1), (42, 2), (42, 4), (43, 1), (43, 2), (43, 4), (43, 7), (43, 6), (43, 5), (44, 1), (44, 2), (44, 3), (44, 4), (44, 7), (44, 10), (45, 1), (45, 2), (45, 3), (45, 4), (46, 1), (46, 2), (46, 5), (46, 4), (47, 1), (47, 2), (47, 4), (47, 5), (47, 6), (47, 10), (47, 7), (48, 1), (48, 2), (48, 4), (48, 10), (49, 2), (49, 3), (49, 4), (49, 6), (50, 1), (50, 2), (50, 4);

INSERT INTO [dbo].[Airports] (AirportCode, StateID) VALUES
/* The States with only 1 International Airport */
('LIT', 4), ('DEN', 6), ('BDL', 7), ('ILG', 8), ('BOI', 12), 
('DSM', 15), ('ICT', 16), ('BWI', 20), ('BOS', 21), ('OMA', 27),
('LAS', 28), ('OKC', 36), ('PDX', 37), ('PVD', 39), ('FSD', 41),
('SLC', 44), ('BTV', 45), ('CRW', 48);

INSERT INTO [dbo].[Airports] (AirportCode, Location, StateID) VALUES
/* Alabama's International Airports */
('HSV', 'N', 1),
('BHM', 'C', 1),
/* Alaska's International Airports */
('FAI', 'N', 2), 
('ANC', 'C', 2),
('PML', 'SW', 2), ('JNU', 'S', 2), ('KTN', 'SE', 2),
/* Arizona's International Airports */
('IFP', 'NW', 3),
('PHX', 'C', 3), 
('YUM', 'SW', 3), ('TUS', 'SE', 3),
/* California's International Airports */
('SFO', 'NW', 5), ('SJC', 'N', 5), ('SMF', 'NE', 5),
('LAX', 'WC', 5), ('FAT', 'C', 5), ('SBD', 'EC', 5),
('SAN', 'SW', 5), ('ONT', 'S', 5), ('PSP', 'SE', 5), 
/* Florida's International Airports */
('PNS', 'NW', 9), ('TLH', 'N', 9), ('DAB', 'NE', 9),  
('TPA', 'WC', 9), ('MCO', 'C', 9), ('FLL', 'EC', 9),
('RSW', 'SW', 9), ('EYW', 'S', 9), ('MIA', 'SE', 9),
/* Georgia's International Airports */
('ATL', 'NW', 10), 
('SAV', 'EC', 10),
/* Hawaii's International Airports */
('HNL', 'NW', 11), 
('KOA', 'SW', 11), ('ITO', 'SE', 11),
/* Illinois's International Airports */
('MLI', 'NW', 13), ('RFD', 'N', 13), ('ORD', 'NE', 13),
('MDW', 'WC', 13), ('PIA', 'C', 13),
/* Indiana's International Airports */
('SBN', 'NW', 14), ('FWA', 'NE', 14),
('IND', 'C', 14),
/* Kentucky's International Airports */
('CVG', 'N', 17), 
('SDF', 'C', 17),
/* Louisiana's International Airports */
('AEX', 'N', 18),
('MSY', 'S', 18),
/* Maine's International Airports */
('BGR', 'EC', 19),
('PWM', 'S', 19),
/* Michigan's International Airports */
('SAW', 'NW', 22), ('MBS', 'N', 22), ('CIU', 'NE', 22),
('GRR', 'WC', 22), ('LAN', 'C', 22), ('FNT', 'EC', 22), 
('AZO', 'SW', 22), ('DTW', 'S', 22),
/* Minnesota's International Airports */
('INL', 'N', 23), ('DLH', 'NE', 23),
('MSP', 'C', 23), 
('RST', 'S', 23),
/* Mississippi's International Airports */
('JAN', 'C', 24), 
('GPT', 'S', 24),
/* Missouri's International Airports */
('MCI', 'NW', 25),
('STL', 'WC', 25),
/* Montana's International Airports */
('GPI', 'NW', 26), ('GTF', 'N', 26),
('MSO', 'WC', 26),
('BZN', 'S', 26), ('BIL', 'SE', 26), 
/* New Hampshire's International Airports */
('MHT', 'S', 29), ('PSM', 'SE', 29),
/* New Jersey's International Airports */
('EWR', 'N', 30), 
('ACY', 'S', 30),
/* New Mexico's International Airports */
('ABQ', 'NW', 31), 
('ROW', 'EC', 31),
/* New York's International Airports */
('PBG', 'NE', 32), ('ART', 'NW', 32),
('BUF', 'WC', 32), ('SYR', 'C', 32), ('ALB', 'EC', 32),
('SWF', 'S', 32), ('JFK', 'SE', 32), 
/* North Carolina's International Airports */
('GSO', 'N', 33), ('RDU', 'NE', 33),
('CLT', 'C', 33), ('ILM', 'EC', 33),
/* North Dakota's International Airports */
('ISN', 'NW', 34), ('MOT', 'N', 34), ('GFK', 'NE', 34),
('DIK', 'SW', 34), ('FAR', 'SE', 34),
/* Ohio's International Airports */
('CLE', 'N', 35),
('CMH', 'C', 35),
('DAY', 'SW', 35),
/* Pennsylvania's International Airports */
('ERI', 'NW', 38), ('AVP', 'NE', 38),
('ABE', 'EC', 38),
('PIT', 'SW', 38), ('MDT', 'S', 38), ('PHL', 'SE', 38), 
/* South Carolina's International Airports */
('GSP', 'NW', 40),
('MYR', 'EC', 40),
('CHS', 'S', 40),  
/* Tennessee's International Airports */
('BNA', 'C', 42), 
('MEM', 'SW', 42),
/* Texas's International Airports */
('LBB', 'NW', 43), ('AMA', 'N', 43), ('DFW', 'NE', 43), 
('ELP', 'WC', 43), ('MAF', 'C', 43), ('SAT', 'EC', 43),
('MFE', 'SE', 43), ('BRO', 'SW', 43),
/* Virginia's International Airports */
('IAD', 'N', 46), ('DCA', 'NE', 46),
('RIC', 'C', 46), 
('ORF', 'SE', 46),
/* Washington's International Airports */
('BLI', 'NW', 47),
('SEA', 'WC', 47), ('GEG', 'EC', 47),
/* Wisconsin's International Airports */
('GRB', 'NE', 49),
('ATW', 'EC', 49),
('MKE', 'SE', 49),
/* Wyoming's International Airports */
('JAC', 'WC', 50), ('CPR', 'C', 50);