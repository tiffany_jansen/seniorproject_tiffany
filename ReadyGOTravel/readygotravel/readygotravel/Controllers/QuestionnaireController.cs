﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using readygotravel.Models;

namespace readygotravel.Controllers
{
    public class QuestionnaireController : Controller
    {
        private DBContext db = new DBContext();
        //pull questionnaire selections from url strings
        List<State> result = new List<State>();
        List<State> climateStates = new List<State>();
        List<State> geographyStates = new List<State>();
        List<State> queryStates = new List<State>();
        string season, weather, temperature, climate, geography;
        string[] tempSplit;
        int[] tempInts = new int[2];
        int temp1;
        int temp2;

        // GET: Questionnaire
        [HttpGet]
        public ActionResult Index()
        {
            season = Request.QueryString["season"];
            weather = Request.QueryString["weather"];
            temperature = Request.QueryString["temperature"];
            climate = Request.QueryString["climate"];
            geography = Request.QueryString["geography"];
            

            if (temperature != null)
            {
                //split temperatue string and convert to ints
                tempSplit = temperature.Split('-');
                tempInts = Array.ConvertAll(tempSplit, s => int.Parse(s));
                temp1 = tempInts[0];
                temp2 = tempInts[1];

                if (climate != "no preference")
                {
                    climateStates = db.Climates.Where(c => c.ClimateType == climate).SelectMany(c => c.States).ToList();
                }
                if (geography != "no preference")
                {
                    geographyStates = db.Geographies.Where(g => g.GeographyType == geography).SelectMany(g => g.States).ToList();
                }
                //switch cases for each season to query the database
                //***FIX THIS -- PARSE OUT EACH SWITCH CASE INTO METHODS***
                switch (season)
                {
                    case "spring":
                        if (climate == "no preference")
                        {
                            foreach (State state in geographyStates)
                            {
                                queryStates.Add(state);
                            }
                            if (weather != "no preference")
                            {
                                SpringWeatherSelected();
                            }
                            else
                            {
                                SpringNoPrefWeather();
                            }
                        }
                        if (geography == "no preference")
                        {
                            foreach (State state in climateStates)
                            {
                                queryStates.Add(state);
                            }
                            if (weather != "no preference")
                            {
                                SpringWeatherSelected();
                            }
                            else
                            {
                                SpringNoPrefWeather();
                            }
                        }
                        if (climate == "no preference" && geography == "no preference")
                        {
                            if (weather != "no preference")
                            {
                                SpringWeatherSelectedStatesDBOnly();
                            }
                            else
                            {
                                SpringNoPrefWeatherStatesDBOnly();
                            }
                        }
                        else
                        {
                            foreach (State state in climateStates)
                            {
                                if (geographyStates.Contains(state))
                                {
                                    queryStates.Add(state);
                                }
                            }
                            if (weather != "no preference")
                            {
                                SpringWeatherSelected();
                            }
                            else
                            {
                                SpringNoPrefWeather();
                            }
                        }
                        ViewBag.Result = "springlist";
                        break;
                    case "summer":
                        if (climate == "no preference")
                        {
                            foreach (State state in geographyStates)
                            {
                                queryStates.Add(state);
                            }
                            if (weather != "no preference")
                            {
                                SummerWeatherSelected();
                            }
                            else
                            {
                                SummerNoPrefWeather();
                            }
                        }
                        if (geography == "no preference")
                        {
                            foreach (State state in climateStates)
                            {
                                queryStates.Add(state);
                            }
                            if (weather != "no preference")
                            {
                                SummerWeatherSelected();
                            }
                            else
                            {
                                SummerNoPrefWeather();
                            }
                        }
                        if (climate == "no preference" && geography == "no preference")
                        {
                            if (weather != "no preference")
                            {
                                SummerWeatherSelectedStatesDBOnly();
                            }
                            else
                            {
                                SummerNoPrefWeatherStatesDBOnly();
                            }
                        }
                        else
                        {
                            foreach (State state in climateStates)
                            {
                                if (geographyStates.Contains(state))
                                {
                                    queryStates.Add(state);
                                }
                            }
                            if (weather != "no preference")
                            {
                                SummerWeatherSelected();
                            }
                            else
                            {
                                SummerNoPrefWeather();
                            }
                        }
                        ViewBag.Result = "summerlist";
                        break;
                    case "fall":
                        if (climate == "no preference")
                        {
                            foreach (State state in geographyStates)
                            {
                                queryStates.Add(state);
                            }
                            if (weather != "no preference")
                            {
                                FallWeatherSelected();
                            }
                            else
                            {
                                FallNoPrefWeather();
                            }

                        }
                        if (geography == "no preference")
                        {
                            foreach (State state in climateStates)
                            {
                                queryStates.Add(state);
                            }
                            if (weather != "no preference")
                            {
                                FallWeatherSelected();
                            }
                            else
                            {
                                FallNoPrefWeather();
                            }
                        }
                        if (climate == "no preference" && geography == "no preference")
                        {
                            if (weather != "no preference")
                            {
                                FallWeatherSelectedStatesDBOnly();
                            }
                            else
                            {
                                FallNoPrefWeatherStatesDBOnly();
                            }
                        }
                        else
                        {
                            foreach (State state in climateStates)
                            {
                                if (geographyStates.Contains(state))
                                {
                                    queryStates.Add(state);
                                }
                            }
                            if (weather != "no preference")
                            {
                                FallWeatherSelected();
                            }
                            else
                            {
                                FallNoPrefWeather();
                            }
                        }
                        ViewBag.Result = "fallresult";
                        break;
                    case "winter":
                        if (climate == "no preference")
                        {
                            foreach (State state in geographyStates)
                            {
                                queryStates.Add(state);
                            }
                            if (weather != "no preference")
                            {
                                WinterWeatherSelected();
                            }
                            else
                            {
                                WinterNoPrefWeather();
                            }

                        }
                        if (geography == "no preference")
                        {
                            foreach (State state in climateStates)
                            {
                                queryStates.Add(state);
                            }
                            if (weather != "no preference")
                            {
                                WinterWeatherSelected();
                            }
                            else
                            {
                                WinterNoPrefWeather();
                            }
                        }
                        if (climate == "no preference" && geography == "no preference")
                        {
                            if (weather != "no preference")
                            {
                                WinterWeatherSelectedStatesDBOnly();
                            }
                            else
                            {
                                WinterNoPrefWeatherStatesDBOnly();
                            }
                        }
                        else
                        {
                            foreach (State state in climateStates)
                            {
                                if (geographyStates.Contains(state))
                                {
                                    queryStates.Add(state);
                                }
                            }
                            if (weather != "no preference")
                            {
                                WinterWeatherSelected();
                            }
                            else
                            {
                                WinterNoPrefWeather();
                            }
                        }
                        ViewBag.Result = "winterresult";
                        break;
                    case "no preference":
                        //no preference
                        if (climate == "no preference")
                        {
                            foreach (State state in geographyStates)
                            {
                                queryStates.Add(state);
                            }
                            if (weather != "no preference")
                            {
                                NoSeasonWeatherSelected();
                            }
                            else
                            {
                                NoSeasonNoPrefWeather();
                            }
                            if (geography == "no preference")
                            {
                                foreach (State state in climateStates)
                                {
                                    queryStates.Add(state);
                                }
                                if (weather != "no preference")
                                {
                                    NoSeasonWeatherSelected();
                                }
                                else
                                {
                                    NoSeasonNoPrefWeather();
                                }
                            }
                            if (climate == "no preference" && geography == "no preference")
                            {
                                if (weather != "no preference")
                                {
                                    NoSeasonWeatherSelectedStatesDBOnly();
                                }
                                else
                                {
                                    NoSeasonNoPrefWeatherStatesDBOnly();
                                }
                            }
                            else
                            {
                                foreach (State state in climateStates)
                                {
                                    if (geographyStates.Contains(state))
                                    {
                                        queryStates.Add(state);
                                    }
                                }
                                if (weather != "no preference")
                                {
                                    NoSeasonWeatherSelected();
                                }
                                else
                                {
                                    NoSeasonNoPrefWeather();
                                }
                            }
                        }
                        ViewBag.Result = "noprefresult";
                        break;
                    default:
                        break;
                }
                if (result.Count() == 0)
                {
                    ViewBag.Message = "I'm sorry, but there are no results for your given selections of " + season + " season, with " + weather.ToLower() + " weather, " + temp1 + " to " + temp2 + " degree temperature, " + climate.ToLower() + " climate, and a geography selection of " + geography.ToLower() + ".";
                }
                else
                {
                    ViewBag.Message = "Here are the results for your selections of " + season + " season, with " + weather.ToLower() + " weather " + temp1 + " to " + temp2 + " degree temperature, " + climate.ToLower() + " climate, and a geography selection of " + geography.ToLower() + ".";
                }
            }

            

            return View(result);
        }

        public void SpringWeatherSelected()
        {
            result = queryStates.Where(s => s.SpringWeather == weather).Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2).ToList();
        }

        public void SpringNoPrefWeather()
        {
            result = queryStates.Where(s => s.SpringWeather == "Sunny" || s.SpringWeather == "Humid" || s.SpringWeather == "Clear skies" || s.SpringWeather == "Cloudy" || s.SpringWeather == "Rainy" || s.SpringWeather == "Snowy").Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2).ToList();
        }

        public void SpringWeatherSelectedStatesDBOnly()
        {
            result = db.States.Where(s => s.SpringWeather == weather).Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2).ToList();
        }

        public void SpringNoPrefWeatherStatesDBOnly()
        {
            result = db.States.Where(s => s.SpringWeather == "Sunny" || s.SpringWeather == "Humid" || s.SpringWeather == "Clear skies" || s.SpringWeather == "Cloudy" || s.SpringWeather == "Rainy" || s.SpringWeather == "Snowy").Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2).ToList();
        }

        public void SummerWeatherSelected()
        {
            result = queryStates.Where(s => s.SummerWeather == weather).Where(s => s.SummerTemp >= temp1 && s.SummerTemp <= temp2).ToList();
        }

        public void SummerNoPrefWeather()
        {
            result = queryStates.Where(s => s.SummerWeather == "Sunny" || s.SummerWeather == "Humid" || s.SummerWeather == "Clear skies" || s.SummerWeather == "Cloudy" || s.SummerWeather == "Rainy" || s.SummerWeather == "Snowy").Where(s => s.SummerTemp >= temp1 && s.SummerTemp <= temp2).ToList();
        }

        public void SummerWeatherSelectedStatesDBOnly()
        {
            result = db.States.Where(s => s.SummerWeather == weather).Where(s => s.SummerTemp >= temp1 && s.SummerTemp <= temp2).ToList();
        }

        public void SummerNoPrefWeatherStatesDBOnly()
        {
            result = db.States.Where(s => s.SummerWeather == "Sunny" || s.SummerWeather == "Humid" || s.SummerWeather == "Clear skies" || s.SummerWeather == "Cloudy" || s.SummerWeather == "Rainy" || s.SummerWeather == "Snowy").Where(s => s.SummerTemp >= temp1 && s.SummerTemp <= temp2).ToList();
        }

        public void FallWeatherSelected()
        {
            result = queryStates.Where(s => s.FallWeather == weather).Where(s => s.FallTemp >= temp1 && s.FallTemp <= temp2).ToList();
        }

        public void FallNoPrefWeather()
        {
            result = queryStates.Where(s => s.FallWeather == "Sunny" || s.FallWeather == "Humid" || s.FallWeather == "Clear skies" || s.FallWeather == "Cloudy" || s.FallWeather == "Rainy" || s.FallWeather == "Snowy").Where(s => s.FallTemp >= temp1 && s.FallTemp <= temp2).ToList();
        }

        public void FallWeatherSelectedStatesDBOnly()
        {
            result = db.States.Where(s => s.FallWeather == weather).Where(s => s.FallTemp >= temp1 && s.FallTemp <= temp2).ToList();
        }

        public void FallNoPrefWeatherStatesDBOnly()
        {
            result = db.States.Where(s => s.FallWeather == "Sunny" || s.FallWeather == "Humid" || s.FallWeather == "Clear skies" || s.FallWeather == "Cloudy" || s.FallWeather == "Rainy" || s.FallWeather == "Snowy").Where(s => s.FallTemp >= temp1 && s.FallTemp <= temp2).ToList();
        }

        public void WinterWeatherSelected()
        {
            result = queryStates.Where(s => s.WinterWeather == weather).Where(s => s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
        }

        public void WinterNoPrefWeather()
        {
            result = queryStates.Where(s => s.WinterWeather == "Sunny" || s.WinterWeather == "Humid" || s.WinterWeather == "Clear skies" || s.WinterWeather == "Cloudy" || s.WinterWeather == "Rainy" || s.WinterWeather == "Snowy").Where(s => s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
        }

        public void WinterWeatherSelectedStatesDBOnly()
        {
            result = db.States.Where(s => s.WinterWeather == weather).Where(s => s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
        }

        public void WinterNoPrefWeatherStatesDBOnly()
        {
            result = db.States.Where(s => s.WinterWeather == "Sunny" || s.WinterWeather == "Humid" || s.WinterWeather == "Clear skies" || s.WinterWeather == "Cloudy" || s.WinterWeather == "Rainy" || s.WinterWeather == "Snowy").Where(s => s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
        }

        public void NoSeasonWeatherSelected()
        {
            result = queryStates.Where(s => s.SpringWeather == weather || s.SummerWeather == weather || s.FallWeather == weather || s.WinterWeather == weather).Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2 || s.SummerTemp >= temp1 && s.SummerTemp <= temp2 || s.FallTemp >= temp1 && s.FallTemp <= temp2 || s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
        }

        public void NoSeasonNoPrefWeather()
        {
            result = queryStates.Where(s => s.SpringWeather == "Sunny" || s.SpringWeather == "Humid" || s.SpringWeather == "Clear skies" || s.SpringWeather == "Cloudy" || s.SpringWeather == "Rainy" || s.SpringWeather == "Snowy" || s.SummerWeather == "Sunny" || s.SummerWeather == "Humid" || s.SummerWeather == "Clear skies" || s.SummerWeather == "Cloudy" || s.SummerWeather == "Rainy" || s.SummerWeather == "Snowy" || s.FallWeather == "Sunny" || s.FallWeather == "Humid" || s.FallWeather == "Clear skies" || s.FallWeather == "Cloudy" || s.FallWeather == "Rainy" || s.FallWeather == "Snowy" || s.WinterWeather == "Sunny" || s.WinterWeather == "Humid" || s.WinterWeather == "Clear skies" || s.WinterWeather == "Cloudy" || s.WinterWeather == "Rainy" || s.WinterWeather == "Snowy").Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2 || s.SummerTemp >= temp1 && s.SummerTemp <= temp2 || s.FallTemp >= temp1 && s.FallTemp <= temp2 || s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
        }

        public void NoSeasonWeatherSelectedStatesDBOnly()
        {
            result = db.States.Where(s => s.SpringWeather == weather || s.SummerWeather == weather || s.FallWeather == weather || s.WinterWeather == weather).Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2 || s.SummerTemp >= temp1 && s.SummerTemp <= temp2 || s.FallTemp >= temp1 && s.FallTemp <= temp2 || s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
        }

        public void NoSeasonNoPrefWeatherStatesDBOnly()
        {
            result = db.States.Where(s => s.SpringWeather == "Sunny" || s.SpringWeather == "Humid" || s.SpringWeather == "Clear skies" || s.SpringWeather == "Cloudy" || s.SpringWeather == "Rainy" || s.SpringWeather == "Snowy" || s.SummerWeather == "Sunny" || s.SummerWeather == "Humid" || s.SummerWeather == "Clear skies" || s.SummerWeather == "Cloudy" || s.SummerWeather == "Rainy" || s.SummerWeather == "Snowy" || s.FallWeather == "Sunny" || s.FallWeather == "Humid" || s.FallWeather == "Clear skies" || s.FallWeather == "Cloudy" || s.FallWeather == "Rainy" || s.FallWeather == "Snowy" || s.WinterWeather == "Sunny" || s.WinterWeather == "Humid" || s.WinterWeather == "Clear skies" || s.WinterWeather == "Cloudy" || s.WinterWeather == "Rainy" || s.WinterWeather == "Snowy").Where(s => s.SpringTemp >= temp1 && s.SpringTemp <= temp2 || s.SummerTemp >= temp1 && s.SummerTemp <= temp2 || s.FallTemp >= temp1 && s.FallTemp <= temp2 || s.WinterTemp >= temp1 && s.WinterTemp <= temp2).ToList();
        }

        public ActionResult Result()
        {
            return View();
        }

        /// <summary>
        /// Use Get to return a view that was passed a State.
        /// </summary>
        /// <param name="stateName">The name of the State that will be passed to the view.</param>
        /// <returns></returns>
        public ActionResult StateInfo(string stateName)
        {
            //Get the state from the db using the states name.
            State selectedState = db.States.Where(n => n.StateName.Equals(stateName)).FirstOrDefault();


            //Make crimeCheck to check what range of level of crime the state falls under.
            decimal crimeRate = selectedState.CrimeRate;
            ViewBag.crimeCheck = CheckCrime(crimeRate);


            //String array for food name items based on state selected
            string[] foodItemsList = new string[3];
            //Fill the Array with appropriate fooditems from the DB
            foodItemsList[0] = db.States.Where(s => s.StateName.Contains(stateName))
                          .Select(f => f.FoodItemOne).FirstOrDefault();
            foodItemsList[1] = db.States.Where(s => s.StateName.Contains(stateName))
                          .Select(f => f.FoodItemTwo).FirstOrDefault();
            foodItemsList[2] = db.States.Where(s => s.StateName.Contains(stateName))
                          .Select(f => f.FoodItemThree).FirstOrDefault();

            ViewBag.foodItems = foodItemsList;

            return View(selectedState);
        }

        //This Method is TESTABLE!!!
        //Returns the ViewBag number for the crimeCheck.
        private int CheckCrime(decimal crimeRate)
        {
            if (crimeRate > 550)
                return 4;
            else if (crimeRate > 400)
                return 3;
            else if (crimeRate > 250)
                return 2;
            else
                return 1;
        }

    }
}