﻿using readygotravel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using readygotravel.Controllers;
using System.Xml;
using System.Net;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using readygotravel.Models.ViewModels;

namespace readygotravel.Controllers
{
    public class TopLocationsController : Controller
    {
        private DBContext db = new DBContext();
        // GET: TopLocations
        public ActionResult Index()
        {
            //Create an array of the top 12 states to reference.
            string[] states = { "WYOMING","HAWAII", "ARIZONA", "NEW YORK", "CALIFORNIA", "MARYLAND", "MASSACHUSETTS",
                "NEVADA","ILLINOIS", "LOUISIANA", "MONTANA", "WASHINGTON"};

            //Crete an array of foodCost, foodItems amounts, and an array of avgHotel Ratings based on Searches.
            decimal[] foodCostList = new decimal[12];
            string[] avgHotelList = new string[12];
            string[] foodItemsList = new string[36];

            //For each of the 12 states, fill the foodcost list/AvgHotelRating List with only states with the specified names.
            for (int i = 0; i < 12; i++)
            {
                //Can not do a db linq query using an array, so used a work around.
                string state = states[i];

                //DB query to pull top state's foodcost data.
                foodCostList[i] = Math.Round(db.States
                    .Where(s => s.StateName.Contains(state))
                    .Select(f => f.FoodCost).FirstOrDefault(), 2);

                //DB queries to pull the three fooditem names per state
                int j = i * 3;
                foodItemsList[j] = db.States.Where(s => s.StateName.Contains(state))
                         .Select(f => f.FoodItemOne).FirstOrDefault();
                foodItemsList[j+1] = db.States.Where(s => s.StateName.Contains(state))
                         .Select(f => f.FoodItemTwo).FirstOrDefault();
                foodItemsList[j+2] = db.States.Where(s => s.StateName.Contains(state))
                         .Select(f => f.FoodItemThree).FirstOrDefault();

                //Try to find results for state searches.
                try
                {
                    //Avg the list of hotel star rating results by the selected state.
                    avgHotelList[i] = Convert.ToString(Math.Round(db.States
                        .Where(s => s.StateName.Contains(state))
                        .SelectMany(a => a.Airports)
                        .SelectMany(n => n.Searches1)
                        .SelectMany(r => r.Results)
                        .Select(r => r.AvgHotelStar)
                        .Average(), 2));
                }

                //If null is returned because of no results for that state, catch and put in a default value.
                catch
                {
                    avgHotelList[i] = "---";
                }
            }

            //ViewBags have stored lists for view page.
            ViewBag.foodList = foodCostList;
            ViewBag.hotelList = avgHotelList;
            ViewBag.foodItems = foodItemsList;
            ViewBag.states = states;

            return View();
        }

        /// <summary>
        /// This involves getting the hotel cost by star rating of a specified state.
        /// </summary>
        /// <param name="id">This represents the number id associated with the number of top locations.</param>
        /// <returns>A JsonResult containing the hotel cost by star rating and the location ID that is is associated with.</returns>
        public JsonResult CostForHotelStars(int? id)
        {
            int check = id ?? default(int);

            //These are the airports codes associated with the states in the top locations page
            //The states in order are: "WYOMING", "HAWAII", "ARIZONA", "NEW YORK", "CALIFORNIA", "MARYLAND", "MASSACHUSETTS", "NEVADA", "ILLINOIS", "LOUISIANA", "MONTANA", "WASHINGTON"}
            string[] AirportCode = { "JAC","HNL", "PHX", "JFK", "LAX", "BWI", "BOS",
                "LAS","ORD", "MSY", "BZN", "SEA"};

            DateTime date1 = DateTime.Now.AddDays(1);
            DateTime date2 = date1.AddDays(7);

            List<decimal> costsByStar = new List<decimal>();

            //Run for every star value.
            for (int i = 1; i < 6; i++)
            {
                List<decimal> hotelstuff = new SearchesController().GetHotelData(date1, date2, 1, AirportCode[check - 1], i);
                costsByStar.Add(hotelstuff.First());
            }
            
            var data = new { locationID = check, CostsByStar = costsByStar};
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        /*
         * This method returns the "Display" View if the logged in user is an admin, otherwise it returns the "Index" View.
         * The Display View shows a table with the Top Searched Locations.         * 
         */ 
        public ActionResult Display()
        {
            //If the user is an admin...
            if (new PeopleController().IsAdminUser(User.Identity))
            {
                //Create a sorted list of top locations, find the total number of items, and display the top 10.
                List<TopSearched> topLocations = GetTopLocations();
                ViewBag.NumSearches = topLocations.Count();
                return View(topLocations.Take(10));
            }
            else
            {
                //Redirect the User to the Index method.
                return RedirectToAction("Index");
            }
            
        }

        /*
         * This method gets the top locations searched. It looks for all the endStates that have been searched and then figures out how many
         * times it was searched. It then returns the list of states in order from most searched to "least" searched.
         * Note: this does not consider places with no search history.
         * 
         * Returns: A sorted list of the Top Searched Locations.
         */ 
        private List<TopSearched> GetTopLocations()
        {
            //Query the end states from the Database
            var endStates = db.Searches
                .GroupBy(s => s.EndAirport)
                .Select(s => s.FirstOrDefault())
                .GroupBy(a => a.Airport1.StateID)
                .Select(a => a.FirstOrDefault())
                .ToList();

            //Create a new List of "TopSearched" items.
            List<TopSearched> top = new List<TopSearched>();

            //Go trhough our state list, state by state.
            foreach (var state in endStates)
            {
                //Count the number of times it's been searched.
                int numSearches = db.Searches.Where(s => s.Airport1.StateID == state.Airport1.StateID)
                    .Select(s => s.Airport1.StateID)
                    .Count();

                //Add the state with the number of times searched to our list to "TopSearched" items.
                top.Add(new TopSearched { EndState = state.Airport1.State.StateName, TimesSearched = numSearches });
            }

            //Sort the list so the top searched state is at the top of the table.
            List<TopSearched> sorted = top
                .OrderByDescending(t => t.TimesSearched)
                .ToList();

            //Return the sorted list.
            return sorted;   
        }

        /*
         * This method is for the Javascript file that adds 10 more rows to the table for the Admin Page.
         * The table holds the Name of the State and the number of times that state was searched as an end state.
         */ 
        public JsonResult AddElements(int? id)
        {
            //Check if there was a number passed in or not. If not, set it to the default int.
            int check = id ?? default(int);

            //Get the list of top locations, skip the number passed in, take the next 10.
            var list = GetTopLocations().Skip(check).Take(10);

            //Create the data using this list. The list conntains "TopSearched" items.
            var data = new { EndState = list.Select(s => s.EndState).ToList(), TimesSearched = list.Select(s => s.TimesSearched).ToList() };

            //Return the Json to the Javascript to be added to the table. 
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}