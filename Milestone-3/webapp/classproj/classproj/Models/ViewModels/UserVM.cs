﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace classproj.Models.ViewModels
{
    public class UserVM
    {
        public UserVM() { }
        public UserVM(string FirstName, string LastName, string Email, string Password)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Email = Email;
            this.Password = Password;
        }

        //The key for the User Table.
        [Key]
        public int UserID { get; set; }

        //The first name of the user. 
        [Display(Name="First Name")]
        public string FirstName { get; set; }

        //The last name of the user.
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        //The email of the user.
        [Display(Name = "Email Address")]
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        //The user's created password.
        [Display(Name = "Password")]
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        //Confirm Password
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

    }
}