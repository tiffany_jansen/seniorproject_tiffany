﻿var ajax_call = function () {

    //get ID for dicussion
    var itemID = $("#DiscussionID").val().toString();
    var source = "/Discussions/CommentRefresh/" + itemID;
    console.log(source);
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: Success,
        error: detectedError
    });
};
ajax_call();
var interval = 1000 * 10;

window.setInterval(ajax_call, interval);

function Success(data) {
    console.log("------success----");

    //data result
    var ComText = data["commentsText"]
    var ComFirst = data["firstNames"]
    var ComLastName = data["lastNames"]
    var TimeStamps = data["TimeStamps"]

    //setup table for comments
    var table = $('<table>', { border: "1", width: "100%", id:'megaTable2', class: 'TableContent' });
    for (i = 0; i < ComText.length; i++) {
        var head1 = $('<tr>')
        var body1 = $('<tr>')

        var name = $('<th>').text(ComFirst + " " + ComLastName + " " + TimeStamps[i]);
        var com = $('<td>', { height: "150" }).text(ComText[i]);

        head1.append(name)
        body1.append(com)

        table.append(head1);
        table.append(body1);

    }
    //replace current table with new one with new comment added
    $('.TableContent').replaceWith(table);
}

function detectedError() {
    console.log("error");
}