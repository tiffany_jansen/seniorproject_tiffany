Sprint 3 Planning Sheet : Team Bits Please!
=======================

## As a user, I would like to know whether or not I am an admin, so I can access the admin pages if I am an admin.
**Tiffany Jansen**, **21 Effort Points**

### Description
* Add Admins to the site
* Have some sort of flag or something show up on Profile Page if the user is an admin
* Figure out how to make this work

### Acceptance Criteria
_If the user is an admin, then there will be a flag or something on the user's profile page._

## As a user, I would like a link to sites where I can book flights and/or hotels so I can book my flight/hotel to the location I want to go.
**Tiffany Jansen**, **2 Effort Points**

### Description
* On the results page, add a link to sites like TripAdvisor or Expedia (Maybe just Google Flights?)

### Acceptance Criteria
_If a user does a search, then the results page will include a link to another travel site where they can book their flight/hotel._

## As an admin, I would like a place to see what destinations are searched the most so I can potentially add that destination to the Top Locations page.
**Tiffany Jansen**, **5 Effort Points**

### Description
* Add a View
    * Make sure only Admins can access it (HOW??)
* Make the view look nice and have a table(?) with the places searched the most 
    * Just the ending airport state?
    * Include the number of times searched.
    * Sort by which was searched the most
        * Greatest at the top of the page.

### Acceptance Criteria
_If an admin goes to the page, then the page will include a table with the destinations searched._
_If a normal user tries to go to the page, then they will be redirected somewhere else (like the home page)._

## As a user I would like to see some of my inputted information on the results page so that I can be sure that the info I entered was correct.
**Tiffany Jansen**, **5 Effort Points**

### Description
On the results page:

* Show the items plugged into the search.
    * "To go from _blank_ airport to _blank_ airport it will cost..."
    * "To get a hotel in (state) near the _blank_ airport it will cost..."
    * "The budget you have to work with is..."
    * "The days you will be traveling are..."
    * etc.
* Make sure it looks nice.

### Acceptance Criteria
_If a user does a search, then the results page will include the inputs from the search page._

## As a user, I would like to know what the weather, temperature, and climate would be like so I can decide if this is a location I would like to potentially travel based on the outside conditions during my stay.
**Tiffany Jansen**, **5 Effort Points**

### Description
On the Details of a State Page:

* Make sure the page looks nice.
    * Add the weather for the state by season
    * Add the temperature for the state by season
    * Add the climate(s) for the state

### Acceptance Criteria
_If a user selects a state, then the page will include the weather, temperature, and climate data._

## As a user, I would like to be able to select what type of climate(s) I like so that I can get some ideas of where I might want to go.
**Stacia Fry**, **13 Effort Points**

### Description
* Populate a database of the climate(s) of each state
* Get this information from a reliable source
* Cite the source on the webpage
* Refactor the database so each state can have multiple climates if needed
* Write the linq queries and update the questionnaire page

### Acceptance Criteria
_If the user queries the database on the questionnaire page including a climate preference, then they will get accurate results based upon their selections from the database._

## As a user, I would like to be able to select what type of geology I like in order to help me decide on a place to travel to.
**Stacia Fry**, **13 Effort Points**

### Description
* Populate a database of the geology(s) of each state
* Get this information from a reliable source
* Cite the source on the webpage
* Refactor the database so each state can have multiple geology if needed
* Write the  queries and update the questionnaire page

### Acceptance Criteria
_If the user queries the database on the questionnaire page including a geology preference, then they will get accurate results based upon their selections from the database._

## As a user, I would like the site's design to reflect the time of day/season.
**Stacia Fry**, **13 Effort Points**

### Description
* Design/write 4 different .css files based upon the seasons (color, images, gifs)
* Write a JavaScript script that updates the website's .css depending on the DateTime/season

### Acceptance Criteria
_If the user visits the website during spring, for example, then the website reflects that season with spring-like colors/images._

## As a user, I would like to see what food options are available so that I can better determine if this would be a location that I would like to travel.
**Adrian Vickers**, **21 Effort Points**

### Description
* Research general cuisine based on all 50 states
* Add said data and research to the DB 
    * Data will be added as a separate table or as attributes of the states table
* Present said results within the "view" below each of the top locations sections
* Adjust the controller to pull said data from the database into the view page

### Acceptance Criteria
_If a user clicks is looking through the view of the top locations page, then the user will be presented with up to 3 different main types of cuisine option listings that are prevalent within each state._

## As a user, I would like to see pictures of the types of food to help me get a better idea of what types of cuisine are available.
**Adrian Vickers**, **8 Effort Points**

### Description
* Find and research pictures of  each of the different cuisine types
* Add pictures to our folder of pics
* Append pics to the top locations panel based on the specific state

### Acceptance Criteria
_If a user clicks on one of the top locations pages, then the user will be presented with up to 3 different main types of cuisine pictures that are prevalent in the state._

## As a user, I would like to know how your information was calculated so that I can better understand how accurate the information is to me.
**Adrian Vickers**, **5 Effort Points**

### Description
* Research other travel company disclaimers
* Create a disclaimer that makes sense for our site
* Append said disclaimer to the correct area of the page for user information
* Format the disclaimer to follow .css the flow of the page

### Acceptance Criteria
_If a user places a search and is presented with results information on the results view page, then a disclaimer should also be presented to the user in a clear manor._

## As a user, I would like to know if there were no flights with my given parameters, so the site doesn't give me an error.
**Adrian Vickers**, **5 Effort Points**

### Description
* Include error catching in all cases where flight data is not returned or no flight information is available.
* Make sure that something explaining this error is returned in the results view page for the user.

### Acceptance Criteria
_If a user puts in data that returns no flights for the given search criteria, then any errors should be caught and the user returned information regarding their failed search criteria._

## As a user, I would like to see more pictures once i click on the link for a popular location so that I can get more of idea of the location and what it looks like.
**Hector Acosta**, **8 Effort Points**

### Description
* 3 pictures per place.
* Make displaying it look good(consider spacing, size, placement, and maybe some sort of way to make it look unique)
* Will need to edit the overlay panel that will display the images since it is currently fixed in place which could restrict the number of things that can be placed on it.

### Acceptance Criteria
_If a user clicks on a location from the "Top Locations" page then he will see more pictures of the area selected._

## As a user, I would like to see a "loading" circle (or other loading indicator) when things are loading so that I know my input was received.
**Hector Acosta**, **5 Effort Points**

### Description
* On the search page (page where you enter your trip details) make a loading circle or other indicator after hitting "search" button.
* might want to clear the page while loading.
* consider making it more interesting than a circle (add image, text, other animation, or etc)

### Acceptance Criteria
_If a user does a search in the search page (page where you enter your trip details) then the screen will display some sort of loading circle/indicator._

## As a user, I would like to know the crime rate of the states, so I can decide if the location is safe enough for me and my travel companions.
**Hector Acosta**, **21 Effort Points**

### Description
* Find API
* Research how to use it
* Make it work
* Consider saving info somewhere (Maybe in a separate table connected to "State")
* display crime rate of location in a location page (get there by clicking on location on questionnaire page).

### Acceptance Criteria
_If a user clicks on a location in the Questionnaire page then they will see the crime rate of the location._

## As a user, I would like to see pictures once I click on the link of a location so that I can get more of idea of the location and what it looks like.
**Hector Acosta**, **5 Effort Points**

### Description
* find pictures for every location we store (50 states so at least 50)
* On a location pages (get to it by clicking on one of the locations after the questionnaire) make it display images of the location
* need to consider where to store images (images in db, stored locally in a file with links in db, on the view page)
* make displaying them look interesting(consider size, spacing, etc.)

### Acceptance Criteria
_If a user clicks on a location after the questionnaire to learn of a location to go to then they will see pictures of the location._

## As a developer, I would like to be able to create a unit test for one of my other PBIs
**Tiffany Jansen**, **5 Effort Points**

### Description
* Add Unit Test

### Acceptance Criteria
_If a developer runs the test, then the test will pass._

## As a developer, I would like to be able to create a unit test for one of my other PBIs
**Stacia Fry**, **5 Effort Points**

### Description
* Add Unit Test

### Acceptance Criteria
_If a developer runs the test, then the test will pass._

## As a developer, I would like to be able to create a unit test for one of my other PBIs
**Adrian Vickers**, **5 Effort Points**

### Description
* Add Unit Test

### Acceptance Criteria
_If a developer runs the test, then the test will pass._

## As a developer, I would like to be able to create a unit test for one of my other PBIs
**Hector Acosta**, **5 Effort Points**

### Description
* Add Unit Test

### Acceptance Criteria
_If a developer runs the test, then the test will pass._