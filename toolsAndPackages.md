# Tools
### Microsoft Visual Studio Community 2017 
version "15.9.5"

### git
version = "2.19.0.1"

### Postman
version="1.8.4.0"

***
# Nuget Packages
### Antlr
version="3.5.0.2"


### bootstrap
version="4.2.1"

### EntityFramework
version="6.2.0"

### jQuery
version="3.3.1"

### jQuery.Validation
version="1.17.0"

### Microsoft.AspNet.Identity.Core
version="2.2.2"

### Microsoft.AspNet.Identity.EntityFramework
version="2.2.2" 

### Microsoft.AspNet.Identity.Owin
version="2.2.2" 

### Microsoft.AspNet.Mvc
version="5.2.7" 

### Microsoft.AspNet.Razor
version="3.2.7" 

### Microsoft.AspNet.TelemetryCorrelation
version="1.0.5" 

### Microsoft.AspNet.Web.Optimization
version="1.1.3" 

### Microsoft.AspNet.WebPages
version="3.2.7" 

### Microsoft.CodeDom.Providers.DotNetCompilerPlatform
version="2.0.1" 

### Microsoft.jQuery.Unobtrusive.Validation
version="3.2.11" 

### Microsoft.Owin
version="4.0.1" 
  
### Microsoft.Owin.Host.SystemWeb
version="4.0.1" 
  
### Microsoft.Owin.Security
version="4.0.1"
  
### Microsoft.Owin.Security.Cookies
version="4.0.1"
  
### Microsoft.Owin.Security.Facebook
version="4.0.1"
  
### Microsoft.Owin.Security.Google
version="4.0.1"
  
### Microsoft.Owin.Security.MicrosoftAccount
version="4.0.1"
  
### Microsoft.Owin.Security.OAuth
version="4.0.1"
  
### Microsoft.Owin.Security.Twitter
version="4.0.1"
  
### Microsoft.Web.Infrastructure
version="1.0.0.0"
  
### Modernizr
version="2.8.3"
  
### Newtonsoft.Json
version="12.0.1"
  
### Owin
version="1.0"
  
### popper.js
version="1.14.3"
  
### System.Diagnostics.DiagnosticSource
version="4.5.1"
  
### WebGrease
version="1.6.0"